#!/usr/bin/python3
""" 
Cleans the bash persistent history of short commands. 
"""

from pathlib import Path
COMMAND_MIN_LENGTH = 5

def shell(cmd):
    return check_output(cmd, shell=True, encoding='utf-8').strip()


def main():
    file_path = Path('./.persistent_history')

    print(f"Cleaning {file_path} from commands shorter than {COMMAND_MIN_LENGTH}.")
    c = input("Continue? (Y/n)")
    if c.strip() != "" and c.lower() != "y":
        return

    with file_path.open('r') as fp:
        lines = fp.readlines()

    print(f"Current number of lines: {len(lines)}")
    updated_lines = []
    for line in lines:
        command = line[21:]
        if len(command) >= COMMAND_MIN_LENGTH:
            updated_lines.append(line)
    with file_path.open('w') as fp:
        fp.writelines(updated_lines)
    print(f"Number of lines after cleaning: {len(updated_lines)}")
    print(f"Cleaned {len(lines) - len(updated_lines)} lines.")


if __name__ == "__main__":
    main()
