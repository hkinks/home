#!/bin/bash
#me=`basename "$0"`
me=bootstrap
currentdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
dir=~/.dotfiles
olddir=~/.dotfiles_old
homedir=~
bindir=$homedir/bin

# Logging functions
function warn {
	echo "[WARN] $1"
}

function info {
	echo "[INFO] $1"
}

function debug {
	if $verbose ; then
		echo "[DEBUG] $1"
	fi
}

# Get arguments
verbose=false
removelinks=false
dryrun=false
while getopts ":vdr" opt; do
  case $opt in
    v)
		verbose=true
		info "Verbose on"
      ;;
    d)
		dryrun=true
		info "Dry run, no links will be created"
      ;;
    r)
		removelinks=true
      ;;
    \?)
		echo "Invalid option: -$OPTARG" >&2
		exit
      ;;
  esac
done


# if backup dir doesn't exist create it
if [ ! -d $olddir ] ; then
	info "Creating $olddir"
	mkdir $olddir
	mkdir $olddir/bin
fi

debug "Working directory $currentdir"

info "Fetching vim Vundle Plugin submodules."
# get submodules
git submodule init
git submodule update
git submodule foreach git pull origin master

# check if current dir is the configured dotfile dir
if [[ ! $currentdir = $dir ]] && [[ -d $currentdir/.git ]]; then
	if [ ! -f $dir ] ; then
		debug "Moving $currentdir to $dir"
		if ! $dryrun ; then 
			cp -rf $currentdir $dir
			rm -rf $currentdir
            cd $dir
		else
			echo "Can't continue with script without $dir existing."
			read -p "Do you want to copy $currentdir to $dir? [y/N] " -n 1 -r
			echo   
			if [[ $REPLY =~ ^[Yy]$ ]]
			then
				cp -rf $currentdir $dir
				rm -rf $currentdir
                cd $dir
			else
				exit 0
			fi
		fi
	else
		warn "Dotfile directory already exists. Stopping"
		exit
	fi
else
	debug "Script is not being run from .git repo"
fi

# check if bin exists
if [ -d $dir/bin ] ; then
	if [ ! -d $bindir ] ; then
		if ! $dryrun ; then 
			mkdir $bindir
		fi
	fi
	# add it to path
	binpath=$bindir
	if [[ :$PATH: == *:$binpath:* ]] ; then
		debug "Directory is in the path, no action taken."
	else
		info "Adding $bindir to path."
		if ! $dryrun ; then 
			export PATH=$PATH:$bindir
		fi
	fi

	# iterate over bin
	for i in $dir/bin/*; do
		file=`basename $i`
		debug "$file (bin)"
		# if exists in user/bin
		if [ -L $bindir/$file ] ; then
			debug "Removing link $bindir/$file"
			if ! $dryrun ; then 
				rm $bindir/$file
			fi
		elif [ -f $bindir/$file ] ; then
			debug "Backupping $bindir/$file to $olddir/bin/"
			if ! $dryrun ; then 
				mv $bindir/$file $olddir/bin/
			fi
		fi
		echo "[LINK] $dir/bin/$file to $bindir"
		if ! $dryrun ; then 
			ln -s $dir/bin/$file $bindir
		fi
	done

	# link bootstrap to bin as well
	if [ -L $bindir/$me ] || [ -f $bindir/$me ] ; then
		debug "$bindir/$me exists. Removing"
		if ! $dryrun ; then 
			rm $bindir/$me
		fi
		echo "[LINK] $dir/$me to $bindir"
		if ! $dryrun ; then 
			ln -s $dir/$me $bindir
		fi
	fi
fi


# iterate over dotfiles
for i in $dir/.*; do
	dotfile=`basename $i`
	## ignore list
	if [[ $dotfile != ".." && $dotfile != ".*"&& $dotfile != "." && $dotfile != .git && $dotfile != .gitignore && $dotfile != *.swp ]] ; then 
		# DIRECTORY
		debug "$dotfile"
		if [ -d $dotfile ] ; then
			debug "$dotfile (dir)"
			#make sure same dir is in home, if not create
			if [ ! -d $homedir/$dotfile ] ; then
				debug "Creating $homedir/$dotfile"
				if ! $dryrun ; then 
					mkdir $homedir/$dotfile
				fi
			fi
			# iterate over files in the dir
			for j in $i/* ; do
				file=`basename $j`
				if [[ $file != "*" ]] ; then 
					debug "$file"
					# if exist, backup
					if [ -L $homedir/$dotfile/$file ] ; then
						debug "$homedir/$dotfile/$file is a link. Removing"
						if ! $dryrun ; then 
							rm $homedir/$dotfile/$file
						fi
					elif [ -d $homedir/$dotfile/$file ] ; then
						debug "$homedir/$dotfile/$file is a dir. Backupping"
						# check if dot-dir exists in backup dir
						if [ ! -d $olddir/$dotfile ] ; then
							debug "Creating $olddir/$dotfile"
							if ! $dryrun ; then 
								mkdir $olddir/$dotfile
							fi
						fi
						if ! $dryrun ; then 
							cp -rf $homedir/$dotfile/$file $olddir/$dotfile/
							rm $homedir/$dotfile/$file -rf
						fi
					elif [ -f $homedir/$dotfile/$file ] ; then
						debug "$homedir/$dotfile/$file is a file. Backupping"
						if [ ! -d $olddir/$dotfile/$dotfile ] ; then
							debug "Creating $olddir/$dotfile"
							if ! $dryrun ; then 
								mkdir $olddir/$dotfile
							fi
						fi
						mv $homedir/$dotfile/$file $olddir/$dotfile
					fi
					echo "[LINK] $dir/$dotfile/$file to $homedir/$dotfile"
					if ! $dryrun ; then 
						ln -s $dir/$dotfile/$file $homedir/$dotfile/
					fi
				fi
			done
		# FILE
		else
			debug "$dotfile (file)"
			if [ -L $homedir/$dotfile ]; then
				debug "$homedir/$dotfile is a link. Removing"
				if ! $dryrun ; then 
					rm $homedir/$dotfile
				fi
			elif [ -f $homedir/$dotfile ] ; then
				debug "$homedir/$dotfile is a file. Backupping"
				# backup
				if ! $dryrun ; then 
					mv $homedir/$dotfile $olddir
				fi
			fi
			echo "[LINK] $dir/$dotfile to $homedir/$dotfile"
			if ! $dryrun ; then 
				ln -s $dir/$dotfile $homedir/$dotfile
			fi
		fi
	else
		debug "Ignoring $dotfile"
	fi
done

# remove broken links
if $removelinks ; then
	info "Removing broken links"
	if ! $dryrun ; then 
		find -L $homedir -type l -delete
	fi
fi

# Move to correct dir
cd $dir

# install vim plugins
vim +PluginInstall +qall

# change back to home directory
cd ~
