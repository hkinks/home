#!/usr/bin/python3

import sys
from pathlib import Path
from subprocess import check_output, run, call
import shutil


FILES = [   '.vimrc', 
            '.bash_profile', 
            '.bashrc', 
            '.vim', 
            '.ssh', 
            '.gitconfig',
            '.config',
            '.tmux.conf',
            '.persistent_history']

LINKS = { 
        # '.bash_profile': '.bashrc' 
        }

def shell(cmd):
    return check_output(cmd, shell=True, encoding='utf-8').strip()

def clean_path(path):
    if path.is_symlink():
        print(f"Unlinking \'{path}\'.")
        path.unlink()
    if path.exists():
        print(f"Removing \'{path}\'.")
        if path.is_dir():
            shutil.rmtree(path)
        else:
            path.unlink()

def main(ask_confirmation=False):
    if len(sys.argv) > 1: 
        home = sys.argv[1]
        print(f"Home path '{home}' was provided as argument.")
    else:
        # try to figure out home path
        home = shell("echo $HOME")
        if home == "$HOME":
            # could not get home path
            home = shell("cd && pwd")

    destination = Path(home)
    source = Path(__file__).parent.parent.absolute() / "configs"
    print("\n#"*60)
    print(f"Source: {source}")
    print(f"Destination: {destination}")
    print("#"*60)

    print("Files to link: ")
    for f in FILES:
        print(f"\t{f}")
    print()

    # confirm
    if ask_confirmation:
        response = input("Proceed with linking? (Y/n)")
        if response != "" and response.lower() != "y":
            return

    print("#"*60)
    print(f"\nStarting to link files...\n")

    for f in FILES:
        source_file = source /f
        if not source_file.exists():
            source_file.touch()

        print()
        dest_file = destination / f
        if dest_file.is_dir():
            if dest_file.is_symlink():
                print(f"[WARN] Directory {dest_file} is currently a symlink. Removing and linking each item in it separately.")
                clean_path(dest_file)
                dest_file.mkdir()

            if not source_file.is_dir():
                print(f"[ERROR] {source_file} is a file, but {dest_file} is a directory. Skipping.")
                continue

            for subdir_file in source_file.iterdir():
                clean_path(dest_file/subdir_file.name)
                print(f"Linking {subdir_file} to {dest_file/subdir_file.name}")
                shell(f"ln -s {subdir_file} {dest_file/subdir_file.name}")

        else:
            clean_path(dest_file)
            print(f"Linking {source_file} to {dest_file}")
            cmd = f"ln -s {source_file} {dest_file}"
            print(cmd)
            shell(cmd)

    for d, s in LINKS.items():
        clean_path(destination / d)
        print(f"Linking {source_file} to {dest_file}")
        shell(f"ln -s {destination / s} {destination / d}")
        print()

    print(f"Removing broken links from {home}")
    result = shell(f"find -L {home} -type l -delete")

    print("Initializing git submodules...")
    result = shell("git submodule init && git submodule update")

    print("Installing vim plugins...")
    result = call("vim +PluginInstall +qall".split())

if __name__ == "__main__":
    main()
    
