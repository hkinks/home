"########## Vundle package manager ############
set encoding=utf-8
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim

" VundleInstall shortcut
map <leader>vi :VundleInstall<CR>:q<CR>

" let Vundle manage Vundle, required
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'

"###########Plugins listing####################
" fuzzy finder
Plugin 'ctrlpvim/ctrlp.vim'
let g:ctrlp_working_path_mode = 0 " don't change working dir all the time
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:ctrlp_show_hidden = 1
set wildignore+=*/tmp/*,*.so,*.swp,*.zip 
"noremap <C-S-o> :CtrlP %:p:h<CR>
noremap <C-b> :CtrlPBuffer<CR>

" commenting out
Plugin 'scrooloose/nerdcommenter.git'
"let g:NERDAltDelims_javascript = 1
"let g:NERDCustomDelimiters = { 'javascript': { 'left': '//', 'leftAlt': '{/*', 'rightAlt': '*/}' }}

set rtp+=~/.fzf
Plugin 'junegunn/fzf'
Plugin 'junegunn/fzf.vim'
map <C-p> :Files .<CR>
map <C-f> :Ag 
map <C-b> :Buffers<CR> 

" json support
Plugin 'elzr/vim-json'

" Markdown
Plugin 'plasticboy/vim-markdown'

" ranger vim 1
"The default shortcut for opening Ranger is <leader>f (\f by default) 
Plugin 'francoiscabrol/ranger.vim'

" Auto resize of panes
Plugin 'roman/golden-ratio'
let g:vim_json_syntax_conceal=0

" Status bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
let g:airline_theme='minimalist'

" Surround text
" https://github.com/tpope/vim-surround.git
" Usage: Visual mode + S
Plugin 'tpope/vim-surround'

" Open URL in browser
" https://github.com/tyru/open-browser.vim
" Usage: gx while cursor on URL
Plugin 'tyru/open-browser.vim'
let g:netrw_nogx = 1 " disable netrw's gx mapping.
nmap gx <Plug>(openbrowser-smart-search)
vmap gx <Plug>(openbrowser-smart-search)

" https://github.com/vim-scripts/vim-auto-save.git
" Plugin 'vim-scripts/vim-auto-save'
" set autowriteall
" au FocusLost * :wa
" let g:auto_save = 1  " enable AutoSave on Vim startup
" let g:auto_save_silent = 1  " do not display the auto-save notification
" let g:auto_save_in_insert_mode = 0  " do not save while in insert mode
" let g:auto_save_no_updatetime = 1 " Do not change the 'updatetime' option
" set updatetime=3000 " 3 sec

" Autoformat
" Needs external plugins as well
"Plugin 'Chiel92/vim-autoformat'
"noremap <F4> :Autoformat<CR>

"###############################################
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"###############################################
"############## Functions ######################

" VIM highlight search and replace
" Escape special characters in a string for exact matching.
" This is useful to copying strings from the file to the search tool
" Based on this - http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
  let string=a:string
  " Escape regex characters
  let string = escape(string, '^$.*\/~[]')
  " Escape the line endings
  let string = substitute(string, '\n', '\\n', 'g')
  return string
endfunction

" Get the current visual block for search and replaces
" This function passed the visual block through a string escape function
" Based on this - https://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
  " Save the current register and clipboard
  let reg_save = getreg('"')
  let regtype_save = getregtype('"')
  let cb_save = &clipboard
  set clipboard&

  " Put the current visual selection in the " register
  normal! ""gvy
  let selection = getreg('"')

  " Put the saved registers and clipboards back
  call setreg('"', reg_save, regtype_save)
  let &clipboard = cb_save

  "Escape any special characters in the selection
  let escaped_selection = EscapeString(selection)

  return escaped_selection
endfunction

" Start the find and replace command across the entire file
vmap <C-r> <Esc>:%s/<c-r>=GetVisual()<cr>//g<left><left>
"Search for selected text
"vnoremap <C-r> "hy:%s/<C-r>h//g<left><left>

command! -nargs=? Filter let @a='' | execute 'g/<args>/y A' | new | setlocal bt=nofile | put! a
"###############################################

"###############################################
"############## Settings #######################
" In iterm2 the colors can get invered. To prevent that:
colorscheme desert
set background=dark

" Case insensitive searches
set ignorecase

" search will be case sensitive if it contains an uppercase letter
set smartcase

" Show how many lines are selected in visual
set showcmd

" don't let changing buffers lose history
set hidden

" switch buffers without saving
set hidden

" reload source
nnoremap <leader>rs :source ~/.vimrc<CR>

" 'jetpack' for buffers
nnoremap gb :ls<CR>:b<Space>

" F3: Toggle list (display unprintable characters).
nnoremap <F3> :set list!<CR>

" Vimlike tab bindings
nnoremap th  :tabfirst<CR>
nnoremap tk  :tabnext<CR>
nnoremap tj  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap tt  :tabedit<Space>
nnoremap tn  :tabnext<Space>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>

" No folding
set nofoldenable

" Split to right
set splitright

" Syntax highlighting
syntax on

" to prevent accidentally pressin K
map K <Nop>

" Makes it keep the indent on the next line
set autoindent

" Numbering of the lines
set number

" Allow saving files as sudo
"command W w !sudo tee % >/dev/null

"timestamp F5
nnoremap <F5> "=strftime("%d.%m.%Y %H:%M:%S")<CR>P
inoremap <F5> <C-R>=strftime("%d.%m.%Y %H:%M:%S")<CR>


"Paste over visually selected text
xnoremap p "_dP

"Smartcase
set smartcase

"Paste in visual mode without copying
xnoremap p pgvy

"map ctrl-backspace to delete previous word
imap <C-BS> <C-W>
inoremap <C-w> <C-\><C-o>dB
inoremap <C-BS> <C-\><C-o>db

" increment remap
nnoremap <A-a> <C-a>
nnoremap <A-x> <C-x>

" pane resize
if bufwinnr(1)
	map + <C-W>+
	map - <C-W>-
endif

" ????
set modeline
set ls=4

"set backspace=2 " make backspace work like most other apps
set backspace=indent,eol,start

" Tab width
set tabstop=4
set shiftwidth=4
set expandtab " convert tabs into spaces

"set clipboard=unnamedplus
" Do not try connecting to the X server to get the current window title and copy/paste using the X clipboard.  
" This avoids a long startup time when running Vim in a terminal emulator and the connection to the X server is slow.  
set clipboard=exclude:.*

nnoremap <A-a> <C-a>
nnoremap <A-x> <C-x>

set wrap
set linebreak
set nolist  " list disables linebreak

" no backup files  .vim.~
set nobackup
" no undo files .vim.un~
set noundofile


"Keybindings that require packages
" Ctrl+/ comment toggle
imap <C-_> <ESC><Leader>c<Space>i
vmap <C-_> <Leader>c<Space>
nmap <C-_> <Leader>c<Space>

" auto set paste
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

function! XTermPasteBegin()
	set pastetoggle=<Esc>[201~
	set paste
	return ""
endfunction

" Show statusbar always
set ttimeoutlen=50
set laststatus=2


" highlight search 
set hlsearch

" no swap
set noswapfile
"###############################################
