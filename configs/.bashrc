##########################################################
####################### ALIASES ##########################
##########################################################
# https://news.ycombinator.com/item?id=10164053
# Append history instead of writing
shopt -s histappend

alias vi=vim
alias ll='ls -artlh'
alias clock='tty-clock -C 4 -c -f %d.%m.%Y'
alias rms='rm .*~ && rm *~'  # remove vim swap files

# include more aliases if added in separate file
if [ -f ~/.bashrc_aliases ]; then
    source ~/.bashrc_aliases
fi

##########################################################
####################### ENV VAR ##########################
##########################################################
export EDITOR=vim

# include env variables specified separately
if [ -f ~/.bashrc_env ]; then
    source ~/.bashrc_env
fi

##########################################################
####################### CONFIG ###########################
##########################################################
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'

	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
	. /etc/bash_completion
fi


if [[ ${EUID} == 0 ]] ; then
	PS1="\[\033[01m\][ \[\033[01;31m\]\u@\h \[\033[00m\]\[\033[01m\]] \[\033[01;32m\]\w\[\033[00m\] \[\033[01;31m\]\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\n\[\033[01;31m\]$\[\033[00m\]> "
else
	PS1="\[\033[01m\][ \[\033[01;34m\]\u@\h \[\033[00m\]\[\033[01m\]] \[\033[01;32m\]\w\[\033[00m\] \[\033[01;31m\]\$(git branch 2>/dev/null | grep '^*' | colrm 1 2)\n\[\033[01;34m\]$\[\033[00m\]> "
fi

set +o vi


if [[ -f "/etc/motd.d/${USER}" && $- == *i* ]]; then # if motd exists AND it's an interactive shell
    /etc/motd.d/${USER};
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Persistent History management
# https://eli.thegreenplace.net/2013/06/11/keeping-persistent-history-in-bash
HISTTIMEFORMAT='[%d-%m-%y %T] '
log_bash_persistent_history()
{
  [[
    $(history 1) =~ ^\ *[0-9]+\ +([^\ ]+\ [^\ ]+)\ +(.*)$
  ]]
  local date_part="${BASH_REMATCH[1]}"
  local command_part="${BASH_REMATCH[2]}"
  if [ "$command_part" != "$PERSISTENT_HISTORY_LAST" ]
  then
    echo $date_part "$command_part" >> ~/.persistent_history
    export PERSISTENT_HISTORY_LAST="$command_part"
  fi
}

bashrc_sourced=$(stat -c %Y ~/.bashrc)

# Stuff to do on PROMPT_COMMAND
run_on_prompt_command()
{
    log_bash_persistent_history
    test $(stat -c %Y ~/.bashrc) -ne $bashrc_sourced && source ~/.bashrc
}

PROMPT_COMMAND="run_on_prompt_command"

alias hgrep='cat ~/.persistent_history|grep --text --color'
alias clean_bs='grep -v "^(\s*(#.*)?)$" -E '

# Used for WSL only
#export DOCKER_HOST=tcp://localhost:2375
